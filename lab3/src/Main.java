import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File filePath = new File("D:/Learning/3/CPP_labs/lab3/src/textFile.txt");
        Scanner scanner = new Scanner(filePath);

        String text = new String();

        while(scanner.hasNextLine()){
            text += scanner.nextLine();
            System.out.println(text);
        }

        String[] words = text.split(" ");
        int digitCount = 0;
        int latinWords = 0;
        int cyrillicWords = 0;
        int othersCount = 0;

        //Збір статистики
        for (int k = 0; k < words.length; k++) {
            for (int i = 0; i < words[k].length(); i++) {
                if (Character.isDigit(words[k].charAt(i))) {
                    while (++i < words[k].length() && (Character.isDigit(words[k].charAt(i)) || words[k].charAt(i) == '.'));
                    if (i == words[k].length()) {
                        digitCount++;

                        //Додавання типу числа після самого числа
                        if (words[k].contains("."))
                            words[k] += "(Real)";
                        else
                            words[k] += "(Integer)";
                    }
                    else
                        othersCount++;

                    continue;
                }

                else if(Character.UnicodeBlock.of(words[k].charAt(i)).equals(Character.UnicodeBlock.CYRILLIC) && Character.isLetter(words[k].charAt(i))) {
                    while(++i < words[k].length()
                            && Character.UnicodeBlock.of(words[k].charAt(i)).equals(Character.UnicodeBlock.CYRILLIC)
                            && (Character.isLetter(words[k].charAt(i)) || words[k].charAt(i) == '.'));
                    if(i == words[k].length())
                        cyrillicWords++;
                    else
                        othersCount++;
                }

                else if(Character.UnicodeBlock.of(words[k].charAt(i)).equals(Character.UnicodeBlock.BASIC_LATIN) && Character.isLetter(words[k].charAt(i))) {
                    while(++i < words[k].length() &&
                            (Character.UnicodeBlock.of(words[k].charAt(i)).equals(Character.UnicodeBlock.BASIC_LATIN))
                            && (Character.isLetter(words[k].charAt(i)) || words[k].charAt(i) == '.'));
                    if(i == words[k].length())
                        latinWords++;
                    else
                        othersCount++;

                    continue;
                }
            }
        }

        text = new String();
        for (String word : words)
            text += word + " ";

        String[] sentences = text.split("\\D\\.");

        int k = 0;
        int arr[] = new int[sentences.length];
        for (int i = 0; i < arr.length; i++)
            arr[i] = 0;

        //Збір статистики по "інших" словах по кожному реченню
        for (String sentence : sentences) {
            words = text.split(" ");
            for (String word : words) {
                for (int i = 0; i < word.length(); i++) {
                    if (Character.isDigit(word.charAt(i))) {
                        while (++i < word.length() && (Character.isDigit(word.charAt(i)) || word.charAt(i) == '.'));
                        if (i != word.length())
                            arr[k]++;

                        continue;
                    }

                    else if(Character.UnicodeBlock.of(word.charAt(i)).equals(Character.UnicodeBlock.CYRILLIC) && Character.isLetter(word.charAt(i))) {
                        while(++i < word.length() && Character.UnicodeBlock.of(word.charAt(i)).equals(Character.UnicodeBlock.CYRILLIC) && Character.isLetter(word.charAt(i)));
                        if(i != word.length())
                            arr[k]++;

                        continue;
                    }

                    else if(Character.UnicodeBlock.of(word.charAt(i)).equals(Character.UnicodeBlock.BASIC_LATIN) && Character.isLetter(word.charAt(i))) {
                        while(++i < word.length()
                                && (Character.UnicodeBlock.of(word.charAt(i)).equals(Character.UnicodeBlock.BASIC_LATIN))
                                && Character.isLetter(word.charAt(i)));
                        if(i != word.length())
                            arr[k]++;

                        continue;
                    }
                }
            }
            k++;
        }

        int maxIndex = 0;
        for(int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[maxIndex])
                maxIndex = i;
        }

        //Виведення тексту без вилученого речення
        for(int i = 0; i < sentences.length; i++) {
            if (i != maxIndex)
                System.out.print(sentences[i] + ".");
        }
        System.out.println("\n\nStatistics:\nLatin words: " + latinWords + "\nCyrillic words: " + cyrillicWords + "\nDigits: "+  digitCount + "\nOther words: " + othersCount);

        text = new String();
        scanner.close();
        scanner = new Scanner(filePath);
        while(scanner.hasNextLine()){
            text += scanner.nextLine();
        }

        //Виведення других по порядку слів, які починаються із заданої літери
        String res = getSecondWords(text.split("\\.\\s{1}"), 's');
        System.out.println(res);
    }

    public static String getSecondWords(String[] sentences, char ch)
    {
        String result = new String();

        for(int i = 0; i < sentences.length; i++) {
            String[] words = sentences[i].split(" ");

            for (String w : words) {
                if (words.length < 2)
                    break;
                Pattern pat = Pattern.compile("\\s*(" + words[1] + ")\\s*");
                Matcher matcher = pat.matcher(w);
                if (matcher.find() && w.charAt(0) == ch) {
                    result += w + " ";
                    break;
                }
            }
        }
        if (result.length() != 0)
            return result;
        else
            return "Error: there is no such words!";
    }
}
