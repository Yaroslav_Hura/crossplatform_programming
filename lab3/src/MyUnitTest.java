import org.junit.Test;
import static org.junit.Assert.*;

public class MyUnitTest {
    @Test
    public void testGetSecondWords() {
        Main main = new Main();
        String[] testStrings1 = new String[]{"String word Earth", "USA world java", "Third string"};
        String[] testStrings2 = new String[]{"String", "USA java", "124 string"};
        String[] testStrings3 = new String[]{"String www", "USA", "Third"};

        assertEquals(main.getSecondWords(testStrings1, 'w'), "word world ");
        assertEquals(main.getSecondWords(testStrings2, 'w'), "Error: there is no such words!");
        assertEquals(main.getSecondWords(testStrings3, 'w'), "www ");
    }
}
