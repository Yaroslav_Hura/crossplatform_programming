package Threads;

import java.lang.*;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;

public class newSwingFrame extends JFrame {

    public JTextArea textArea;
    public JButton buttonDoWork = new JButton("Start");

    public PrintStream printOut;

    public newSwingFrame() {
        super("Lab 6");

        textArea = new JTextArea(50, 10);
        textArea.setEditable(false);
        PrintStream printStream = new PrintStream(new newPrintStream(textArea));

        // переспрямування потоків виводу та помилок
        System.setOut(printStream);
        System.setErr(printStream);

        // створення GUI
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        //параметри для кнопки та її додавання на форму
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        add(buttonDoWork, constraints);

        //параметри для текстового поля та його розміщення на формі
        constraints.gridx = 1;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        add(new JScrollPane(textArea), constraints);

        // прив'язка обробника події натискання кнопки Start
        buttonDoWork.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                printLog();
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 340); // розміри вікна
        setLocationRelativeTo(null);    // центрування
    }

    //Prints log statements for testing in a thread
    public void printLog() {
        //GaussianElimination newGaus = new GaussianElimination();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("" + (new GaussianElimination()));
                    System.out.println("" + (new Monitor()));
                    try {
                        Thread.sleep(100000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new newSwingFrame().setVisible(true);
            }
        });
    }
/*
    static class Table{
        synchronized void printTable(int n){//synchronized method
            for(int i=1;i<=5;i++){
                System.out.println(n*i);
                try{
                    Thread.sleep(400);
                }catch(Exception e){System.out.println(e);}
            }

        }
    }

    public class TestSynchronization3{
        public void main(String args[]){
            final Table obj = new Table();//only one object

            Thread t1=new Thread(){
                public void run(){
                    obj.printTable(-1);
                }
            };
            Thread t2=new Thread(){
                public void run(){
                    obj.printTable(2);
                }
            };
            t1.start();
            t2.start();
        }
    }

    public static void main(String args[]){
        final Table obj = new Table();//only one object

        Thread t1=new Thread(){
            public void run(){
                obj.printTable(-1);
            }
        };
        Thread t2=new Thread(){
            public void run(){
                obj.printTable(1);
            }
        };
        t1.start();
        t2.start();
    }*/
}

