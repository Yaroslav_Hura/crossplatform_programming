package Threads;

public class GaussianElimination extends Thread {
    private static final double EPSILON = 1e-10;

    public String res;
    public String resID;
    public String resName;
    public String resPriority;
    public String resIsAlive;

    // обчислення методом Гауса
    public double[] lsolve(double[][] A, double[] b) {
        int n = b.length;

        for (int p = 0; p < n; p++) {

            //пошук рядка із найбільшим p-тим елементом
            int max = p;
            for (int i = p + 1; i < n; i++) {
                if (Math.abs(A[i][p]) > Math.abs(A[max][p])) {
                    max = i;
                }
            }
            // перестановка рядка із найбільшим p-тим значенням
            double[] temp = A[p]; A[p] = A[max]; A[max] = temp;
            double   t    = b[p]; b[p] = b[max]; b[max] = t;

            // якщо матриця одинична
            if (Math.abs(A[p][p]) <= EPSILON) {
                throw new ArithmeticException("Matrix is singular or nearly singular");
            }

            // зміна і-того рядка матриць А та B
            for (int i = p + 1; i < n; i++) {
                double alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < n; j++) {
                    A[i][j] -= alpha * A[p][j];
                }
            }
        }

        // зворотній хід
        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < n; j++) {
                sum += A[i][j] * x[j];
            }
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

    // cинхронізований метод, який обчислює та виводить результат
    public synchronized void mainFunc() {
        int n = 3;
        double[][] A = {
                { 0, 1,  1 },
                { 2, 4, -2 },
                { 0, 3, 15 }
        };
        double[] b = { 4, 2, 36 };
        double[] x = lsolve(A, b);

        System.out.println("Gaussian info: \nId: "+Thread.currentThread().getId()
                +"; Priority: " +Thread.currentThread().getPriority()
                +"; Name: " +Thread.currentThread().getName()
                +"; isAlive: "+Thread.currentThread().isAlive()
                +"; State: "+Thread.currentThread().getState()
                +"\n");

        this.resID = String.valueOf(Thread.currentThread().getId());
        this.resPriority = String.valueOf(Thread.currentThread().getPriority());
        this.resName = Thread.currentThread().getName();
        this.resIsAlive = String.valueOf(Thread.currentThread().isAlive());
    }

    public void run() {
        mainFunc();
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void GaussianElimination()
    {

    }
}
