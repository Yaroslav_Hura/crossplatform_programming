package Threads;

import java.util.Timer;
import java.util.TimerTask;

public class Monitor extends Thread {
    static int counter = 0;

    public GaussianElimination gausInstance;

    public Monitor(GaussianElimination gaus)
    {
        gausInstance = gaus;
    }

    public Monitor() {
        makeString();
    }

    public static String smth()
    {
        int n = 3;
        double[][] A = {
                { 0, 1,  1 },
                { 2, 4, -2 },
                { 0, 3, 15 },
        };
        double[] b = { 4, 2, 36 };
        GaussianElimination gaus = new GaussianElimination();
        gaus.start();
        double[] x = gaus.lsolve(A, b);
        String var = new String();

        for (int i = 0; i < n; i++) {
            var += x[i]+" ";
        }

        return var;
    }

    public static void makeString()
    {
        String newStr = smth();
        String delims = "[ ]+";
        String[] tokens = newStr.split(delims);

        for(int i=0; i<tokens.length; i++)
        {
            try
            {
                System.out.println("\nMonitor info: \nId: "+Thread.currentThread().getId()
                        +"; Priority: " +Thread.currentThread().getPriority()
                        +"; Name: " +Thread.currentThread().getName()
                        +"; isAlive: "+Thread.currentThread().isAlive()
                        +"; State: "+Thread.currentThread().getState()
                        +"; Value: " +tokens[i]);

                Timer timer = new Timer("MyTimer");//create a new Timer

                TimerTask timerTask = new TimerTask() {

                    @Override
                    public void run() {
                        System.out.println("TimerTask executing counter is: " + counter);
                        if (counter < 2) {
                        counter++;//increments the counter
                        }
                        else{
                            timer.cancel();//end the timer
                            System.out.println("Counter of thread has reached 3 now will terminate");
                        }
                    }
                };

                timer.scheduleAtFixedRate(timerTask, 3, 3000);//this line starts the timer at the same time its executed

                if (counter == 3) {
                    timer.cancel();//end the timer
                    timerTask.cancel();
                }

                Thread.sleep(1000);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
    }

    public void run() {
        makeString();
    }
}
