package com.company;

public abstract class Appliance {
    protected int power;
    protected boolean enabled;

    Appliance() {
        this.power = 0;
        this.enabled = false;
    }

    Appliance(int power, boolean state) {
        this.power = power;
        this.enabled = state;
    }

    public int getPower() {
        return power;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void turnOn() {
        this.enabled = true;
    }

    public void turnOff() {
        this.enabled = false;
    }

    public String getInfo() {
        return String.format("Device type: %s\tPower: %s\tState: %s", this.getClass().getSimpleName(), power, enabled);
    }
}
