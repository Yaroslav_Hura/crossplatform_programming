package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ArrayList<Appliance> list = new ArrayList<Appliance>();

        list.add(new Microwave(1000, false, 120, 70 ));
        list.add(new Kettle(2100, false, 100, 1.8 ));
        list.add(new Fridge(3000, false, -5, 45.5 ));
        list.add(new Microwave(900, false, 100, 70 ));
        list.add(new Fridge(2000, false, -4, 70 ));
        list.add(new Microwave(1300, false, 100, 60 ));
        list.add(new Kettle(1700, false, 80, 1.6 ));
        list.add(new Microwave(1200, false, 60, 40 ));

        ApplianceManager appManager = new ApplianceManager(list);
        System.out.println("Created list:");
        System.out.println(appManager.getAppliancesList());

        List<Integer> listToEnable = Arrays.asList(1, 3, 4, 6 ,7);

        appManager.turnOn(listToEnable);
        System.out.println("Some appliances were enabled.\n\nList of enabled appliances:");
        System.out.println(appManager.getAppliancesList());
        System.out.println("Total power of enabled appliances: " + appManager.calculateConsumedPower() + '\n');


        System.out.println("Before sorting:");
        System.out.println(appManager.getAppliancesList());

        appManager.sortByPowerDescending();
        System.out.println(appManager.getAppliancesList());

        appManager.sortByPowerAscending();
        System.out.println(appManager.getAppliancesList());

        appManager.sortByTypeDescending();
        System.out.println(appManager.getAppliancesList());

        appManager.sortByTypeAscending();
        System.out.println(appManager.getAppliancesList());
    }
}
