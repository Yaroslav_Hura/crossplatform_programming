package com.company;

public class Microwave extends Appliance {
    private int timer;
    private int temperature;

    Microwave() {
        this.temperature = 0;
        this.timer = 0;
    }

    Microwave(int power, boolean state, int timer, int temperature) {
        this.power = power;
        this.enabled = state;
        this.timer = timer;
        this.temperature = temperature;
    }

    public void setTimer(int time) {
        this.timer = time;
    }

    public int getTime() {
        return timer;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public  int getTemperature() {
        return temperature;
    }

    public String getInfo() {
        return String.format("Device type: %-12s Power: %-8s State: %-10s Timer: %-11s Temperature: %-8s",
                this.getClass().getSimpleName(), power, enabled ? "enabled" : "disabled", timer, temperature);
    }
}
