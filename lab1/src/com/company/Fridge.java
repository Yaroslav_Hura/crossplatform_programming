package com.company;

public class Fridge extends Appliance {
    private int temperature;
    private double volume;

    Fridge(int power, boolean state, int temperature, double volume) {
        this.power = power;
        this.enabled = state;
        this.temperature = temperature;
        this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getInfo() {
        return String.format("Device type: %-12s Power: %-8s State: %-10s Volume: %-10s Temperature: %-8s",
                this.getClass().getSimpleName(), power, enabled ? "enabled" : "disabled", volume, temperature);
    }
}
