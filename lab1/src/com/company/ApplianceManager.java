package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;

public class ApplianceManager {
    private  ArrayList<Appliance> applianceList;
    private Comparator<Appliance> powerComparatorByDescending = (Appliance a1, Appliance a2) -> (a1.getPower() - a2.getPower());

    ApplianceManager(ArrayList<Appliance> applianceList) {
        this.applianceList = applianceList;
    }

    public void turnOn(List<Integer> indexes) {
        for (Integer i: indexes) {
            if (i < applianceList.size()) {
                applianceList.get(i).turnOn();
            }
        }
        applianceList.removeIf(n -> !n.isEnabled());
    }

    public void turnOff(List<Integer> indexes) {
        for (Integer i: indexes) {
            if (i < applianceList.size()) {
                applianceList.get(i).turnOff();
            }
        }
    }

    public int calculateConsumedPower() {
        int totalPower = 0;

        for (Appliance appliance: applianceList) {
            if (appliance.isEnabled()) {
                totalPower += appliance.getPower();
            }
        }

        return totalPower;
    }

    public static class TypeComparatorByDescending implements Comparator<Appliance> {
        @Override
        public int compare(Appliance a1, Appliance a2) {
            return a1.getClass().getSimpleName().compareTo(a2.getClass().getSimpleName());
        }
    }

    public class TypeComparatorByAscending implements Comparator<Appliance> {
        @Override
        public int compare(Appliance a1, Appliance a2) {
            return a2.getClass().getSimpleName().compareTo(a1.getClass().getSimpleName());
        }
    }

    public String getAppliancesList()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (Appliance appliance: applianceList) {
            stringBuilder.append(appliance.getInfo() + '\n');
        }

        return stringBuilder.toString();
    }

    public void sortByPowerDescending() {
        System.out.println("Sorting by power descending:\n");
        Collections.sort(applianceList, powerComparatorByDescending);
    }

    public void sortByPowerAscending() {
        System.out.println("Sorting by power ascending:\n");
        Collections.sort(applianceList, new Comparator<Appliance>() {
            @Override
            public int compare(Appliance a1, Appliance a2) {
                return a2.getPower() - a1.getPower();
            }
        });
    }

    public void sortByTypeDescending() {
        System.out.println("Sorting by type descending:\n");
        Collections.sort(applianceList, new TypeComparatorByDescending());
    }

    public void sortByTypeAscending() {
        System.out.println("Sorting by type ascending:\n");
        Collections.sort(applianceList, new TypeComparatorByAscending());
    }
}
