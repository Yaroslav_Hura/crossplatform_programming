import java.time.LocalDate;
import java.util.Objects;

public class HistoricalEvent {
    String name;
    LocalDate date;

    public HistoricalEvent(String eventName, LocalDate date) {
        this.name = eventName;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("%s  %s", date, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoricalEvent event = (HistoricalEvent) o;
        return name.equals(event.name) &&
                date.equals(event.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, date);
    }
}
