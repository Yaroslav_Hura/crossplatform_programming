import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        String file = "src/textFiles/input1.txt";
        Scanner scanner = new Scanner(new File(file));
        scanner.useDelimiter(";");
        ArrayList<HistoricalEvent> eventsOf2001 = new ArrayList<HistoricalEvent>();
        ArrayList<HistoricalEvent> eventsOf2002 = new ArrayList<HistoricalEvent>();
        ArrayList<HistoricalEvent> eventsOf2003 = new ArrayList<HistoricalEvent>();
        Scanner in = new Scanner(System.in);

        System.out.println("******Lab2 Main menu******");
        System.out.println("Enter number of task to do (1 (includes 2) or 3)");

        switch(in.nextLine()){
            case "1":
                for (int i = 0; i < 5 && scanner.hasNext(); i++)
                {
                    eventsOf2001.add(new HistoricalEvent(scanner.next(), LocalDate.parse(scanner.next())));
                }

                for (int i = 0; i < 5 && scanner.hasNext(); i++)
                {
                    eventsOf2002.add(new HistoricalEvent(scanner.next(), LocalDate.parse(scanner.next())));
                }

                for (int i = 0; i < 5 && scanner.hasNext(); i++)
                {
                    eventsOf2003.add(new HistoricalEvent(scanner.next(), LocalDate.parse(scanner.next())));
                }

                scanner.close();
                Map<Integer, ArrayList<HistoricalEvent>> eventMap = new HashMap<Integer, ArrayList<HistoricalEvent>>();
                eventMap.put(2001, eventsOf2001);
                eventMap.put(2002, eventsOf2002);
                eventMap.put(2003, eventsOf2003);

                System.out.println("\n\nEvents of 2001 year:\n");
                for ( Object a : eventMap.get(2001).stream().sorted(Comparator.comparing(HistoricalEvent::getDate)).toArray()){
                    System.out.println(a);
                }
                System.out.println("\n\nEvents of 2002 year:\n");
                for ( Object a : eventMap.get(2002).stream().sorted(Comparator.comparing(HistoricalEvent::getDate)).toArray()){
                    System.out.println(a);
                }
                System.out.println("\n\nEvents of 2003 year:\n");
                for ( Object a : eventMap.get(2003).stream().sorted(Comparator.comparing(HistoricalEvent::getDate)).toArray()){
                    System.out.println(a);
                }

                ArrayList<HistoricalEvent> events2001 = new ArrayList<>(eventMap.get(2001));
                ArrayList<HistoricalEvent> events2002 = new ArrayList<>(eventMap.get(2002));
                ArrayList<HistoricalEvent> events2003 = new ArrayList<>(eventMap.get(2003));
                events2001.sort(Comparator.comparing(HistoricalEvent::getDate));
                events2002.sort(Comparator.comparing(HistoricalEvent::getDate));
                events2003.sort(Comparator.comparing(HistoricalEvent::getDate));

                ArrayList<HistoricalEvent> listToRemove = new ArrayList<>();
                //events2001 pairs removing
                for(int i = 0; i < eventsOf2001.size() - 1; i++)
                {
                    if (events2001.get(i).getDate().equals(events2001.get(i + 1).getDate().minusDays(1))) {
                        listToRemove.add(events2001.get(i));
                        listToRemove.add(events2001.get(i + 1));
                    }
                }
                for (HistoricalEvent e : listToRemove) {
                    events2001.remove(e);
                }
                listToRemove.clear();
                //events2002 pairs removing
                for(int i = 0; i < eventsOf2002.size() - 1; i++)
                {
                    if (events2002.get(i).getDate().equals(events2002.get(i + 1).getDate().minusDays(1))) {
                        listToRemove.add(events2002.get(i));
                        listToRemove.add(events2002.get(i + 1));
                    }
                }
                for (HistoricalEvent e : listToRemove) {
                    events2002.remove(e);
                }
                listToRemove.clear();
                //events2003 pairs removing
                for(int i = 0; i < eventsOf2003.size() - 1; i++)
                {
                    if (events2003.get(i).getDate().equals(events2003.get(i + 1).getDate().minusDays(1))) {
                        listToRemove.add(events2003.get(i));
                        listToRemove.add(events2003.get(i + 1));
                    }
                }
                for (HistoricalEvent e : listToRemove) {
                    events2003.remove(e);
                }

                System.out.println("\n\nReduced lists:\n\nEvents of 2001 year:\n");
                for (HistoricalEvent e : events2001)
                {
                    System.out.println(e);
                }
                System.out.println("\n\nEvents of 2002 year:\n");
                for (HistoricalEvent e : events2002)
                {
                    System.out.println(e);
                }
                System.out.println("\n\nEvents of 2003 year:\n");
                for (HistoricalEvent e : events2003)
                {
                    System.out.println(e);
                }

                break;
            case "3":
                String file1 = "src/textFiles/input2.txt";
                String file2 = "src/textFiles/input3.txt";

                scanner = new Scanner(new File(file1));
                scanner.useDelimiter(";");
                ArrayList<HistoricalEvent> list1 = new ArrayList<>();
                ArrayList<HistoricalEvent> list2 = new ArrayList<>();

                while (scanner.hasNext())
                {
                    list1.add(new HistoricalEvent(scanner.next(), LocalDate.parse(scanner.next())));
                }

                scanner.close();
                scanner = new Scanner(new File(file2));
                scanner.useDelimiter(";");

                while (scanner.hasNext())
                {
                    list2.add(new HistoricalEvent(scanner.next(), LocalDate.parse(scanner.next())));
                }

                HashSet<HistoricalEvent> uniqueDates = new HashSet<>(list1);
                uniqueDates.removeAll(list2);

                System.out.println("\n\nUnique elements of 1 list:\n");
                for (HistoricalEvent e : uniqueDates)
                {
                    System.out.println(e);
                }

                //Pairs search
                HashMap<HistoricalEvent, ArrayList<HistoricalEvent>> result = new HashMap<>();
                ArrayList<HistoricalEvent> arr = new ArrayList<>(uniqueDates);
                arr.sort(Comparator.comparing(HistoricalEvent::getDate));
                for (int i = 0; i < arr.size() - 1; i++)
                {
                    ArrayList<HistoricalEvent> values = new ArrayList<>();
                    for (int j = i + 1; j < arr.size(); j++)
                    {
                        if(arr.get(i).getDate().plusYears(1).isBefore(arr.get(j).getDate()))
                            values.add(arr.get(j));
                    }
                    result.put(arr.get(i), values);
                }
                System.out.println("\n\nResult\n");
                System.out.println(Collections.singletonList(result));

                break;
            default:
                System.out.println("Wrong number!");
        }
    }
}
